// Чому для роботи з input не рекомендується використовувати клавіатуру?
// Input може бути заповнений не лише за допомогою клавіатури, наприклад голосові команди або копіювання і вствка за допомогою миші,
// при цому клавіатура буде не задіяна.

const btnCollection = document.querySelectorAll(".btn");

window.addEventListener("keydown", (e) => {
  btnCollection.forEach((elem) => {
    elem.dataset.key == e.code.toString().toLowerCase().slice(3) ||
    elem.dataset.key == e.code.toString().toLowerCase()
      ? (elem.className = "btn-active")
      : (elem.className = "btn");
  });
});
